const app = require("../app");
const request = require('supertest');

describe("API /items", () => {

    it("test get /items", (done) => {
        request(app)
            .get("/items")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                console.log(response.body)
                const firstData = response.body[0]
                expect(firstData.id).toBe(1)
                expect(firstData.name).toBe('Meja belajar')
                expect(firstData.status).toBe("active")
                done();
            })
            .catch(done)
    })

    it("test get /items/:id", (done) => {
        
        request(app)
            .get("/items/2")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const data = response.body;
                expect(data.id).toBe(2)
                expect(data.name).toBe('Playstation')
                expect(data.status).toBe('inactive')
                expect(data.type).toBe('Gaming')
                done();
            })
            .catch(done)
    })

    it("test post /items", (done) => {

        request(app)
            .post("/items")
            .send({ name: "Mouse", status: "active", type: "Electronic"})
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)
            .then(response => {
                const data = response.body;
                expect(data.name).toBe("Mouse")
                expect(data.status).toBe("active")
                expect(data.type).toBe("Electronic")
                done();
            })
            .catch(done)
    })

    it("delete post /items/:id", (done) => {

        request(app)
            .delete("/items/3")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const data = response.body;
                expect(data.message).toBe("Delete successfully")
                done();
            })
            .catch(done)
    })



    
})
