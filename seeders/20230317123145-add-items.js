'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    
    await queryInterface.bulkInsert('Items', [
      {
        name: "Meja belajar",
        status: "active",
        type: "Furniture",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "Playstation",
        status: "active",
        type: "Gaming",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "Mobil",
        status: "active",
        type: "Vehicle",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Items', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
